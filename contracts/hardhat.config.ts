import "@nomicfoundation/hardhat-toolbox";
import 'module-alias/register'
import { HardhatUserConfig, task } from 'hardhat/config'
import 'hardhat-deploy'
import 'hardhat-gas-reporter'
import 'solidity-coverage'
import 'hardhat-contract-sizer'
import '@typechain/hardhat'
import { ethers } from 'hardhat'
import * as dotenv from 'dotenv'
dotenv.config()

task('accounts', 'Prints the list of accounts', async (_, hre) => {
  const accounts = await hre.ethers.getSigners()
  for (const account of accounts) console.log(account.address)
})

task('wallets', 'Create new wallet', async (_, hre) => {
  for (let i = 0; i < 5; i++) {
    const wallet = hre.ethers.Wallet.createRandom()
    console.log({
      address: wallet.address,
      privateKey: wallet.privateKey,
    })
  }
})

const config: HardhatUserConfig = {
  solidity: {
    version: "0.8.18",
    settings: {
      optimizer: {
        enabled: true,
        runs: 200
      }
    }
  },
  networks: {
    hardhat: {
      tags: ['localhost'],
      deploy: ['deploy/localhost/'],
    },
    // place here any network you like
    // bsc_testnet: {
    //   tags: ['testnet'],
    //   deploy: ['deploy/testnet/'],
    //   url: 'https://bsc-testnet.blockpi.network/v1/rpc/public',
    //   accounts: process.env.PRIVATE_TEST?.split(',') || [],
    //   verify: {
    //     etherscan: {
    //       apiKey: process.env.API_BSC || ''
    //     }
    //   }
    // }
  },
  gasReporter: {
    enabled: true,
    currency: 'USD',
  },
  contractSizer: {
    alphaSort: true,
    disambiguatePaths: false,
    runOnCompile: true,
  },
}

export default config
