import { Tier } from '@/test/helper';
import { deployments, ethers } from 'hardhat'
import type { DeployFunction } from 'hardhat-deploy/types'
import { HardhatRuntimeEnvironment } from 'hardhat/types';
import { getContract } from '../helper';
import { NodeNFT, SellerUtils, Token } from '@/typechain-types';
import { User, toLeaves } from '@/test/parse';
import { calculateRoot } from '@/test/merkle';
import { deployProxy } from '@/test/proxyHelper';

import fs from 'fs'

const func: DeployFunction = async (hre: HardhatRuntimeEnvironment) => {
    const { deploy } = deployments;
    const [deployer, collector, user] = await ethers.getSigners()

    const users = [user.address];
    const leaves = toLeaves(users as User[])
    const root = calculateRoot(leaves)

    const block = await ethers.provider.getBlock('latest')
    const now = BigInt(Date.now()) / 1000n

    const start = (BigInt(block?.timestamp || now)) + 3600n
    const end = start + 86400n * 10n

    let tiers: Tier[] = []

    const ONE = ethers.WeiPerEther

    tiers.push({
        supply: 100n,
        capacityPerAddress: 5n,
        pricePerNode: ONE
    } as Tier)

    tiers.push({
        supply: 50n,
        capacityPerAddress: 10n,
        pricePerNode: ONE * 5n
    } as Tier)

    tiers.push({
        supply: 10n,
        capacityPerAddress: 10n,
        pricePerNode: ONE * 10n
    } as Tier)

    const nft = await getContract('NodeNFT') as NodeNFT
    const token = await getContract('Token') as Token
    const sellerUtils = await getContract('SellerUtils') as SellerUtils

    // await deploy('Seller', {
    //     contract: 'Seller',
    //     args: [],
    //     proxy: {
    //         owner: deployer.address,
    //         proxyContract: 'OpenZeppelinTransparentProxy',
    //         execute: {
    //             methodName: 'init',
    //             args: [
    //                 nft.target, token.target, root, 'leaves/link', start, end, (end - start) / 2n, tiers, deployer.address, collector.address
    //             ],
    //         },
    //     },
    //     from: deployer.address,
    //     libraries: {
    //         'SellerUtils': await sellerUtils.getAddress()
    //     },
    //     log: true
    // })

    const libraries = {
        'SellerUtils': await sellerUtils.getAddress()
    }

    const seller = await deployProxy('Seller', deployer.address, deployer, [nft.target, token.target, root, 'leaves/link', start, end, (end - start) / 2n, tiers, deployer.address, collector.address], libraries, 'init')
    
    const txres = await seller.proxy.waitForDeployment()

    console.log(`Deployed Seller : ${await seller.proxy.getAddress()}`)
    const data = {
        address: seller.proxy.target,
        abi: seller.proxy.interface
    }
    fs.writeFileSync('./Seller.json', JSON.stringify(data))
}
export default func

func.tags = ['Seller.deploy']
func.dependencies = ['Token.deploy', 'NFT.deploy', 'SellerUtils.deploy']
