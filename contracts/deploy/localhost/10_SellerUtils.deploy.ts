import { deployments, ethers } from 'hardhat'
import type { DeployFunction } from 'hardhat-deploy/types'
import { HardhatRuntimeEnvironment } from 'hardhat/types';

const func: DeployFunction = async (hre: HardhatRuntimeEnvironment) => {
    const { deploy } = deployments;
    const [deployer] = await ethers.getSigners()

    await deploy('SellerUtils', {
        contract: 'SellerUtils',
        args: [],
        from: deployer.address,
        log: true
    })
}
export default func

func.tags = ['SellerUtils.deploy']
