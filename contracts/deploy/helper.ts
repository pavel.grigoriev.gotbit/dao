import { BaseContract } from 'ethers'
import {ethers, deployments} from 'hardhat'

export const getContract = async (name: string) => {
    const contractDeployment = await deployments.get(name)
    const contract = await ethers.getContractAt(name, contractDeployment.address)
    return contract as BaseContract 
}