//SPDX-License-Identifier: MIT
pragma solidity =0.8.18;

import '@openzeppelin/contracts/token/ERC20/ERC20.sol';
import '@openzeppelin/contracts/access/Ownable.sol';

/**
 * @title Token
 * @author kotsmile
 */

contract Token is ERC20, Ownable {
    constructor(string memory name, string memory symbol) ERC20(name, symbol) {
        _mint(msg.sender, 1_000_000_000 ether);
    }

    function mint(address to, uint256 amount) external onlyOwner {
        _mint(to, amount);
    }

    function burn(address from, uint256 amount) external onlyOwner {
        _burn(from, amount);
    }
}

contract TokenWithDecimals is ERC20, Ownable {
    uint8 private immutable _decimals;

    constructor(string memory name, string memory symbol, uint256 decimals) ERC20(name, symbol) {
        _decimals = uint8(decimals);
        _mint(msg.sender, 1_000_000_000 ether);
    }

    function decimals() public override view returns(uint8) {
        return _decimals;
    }
}
