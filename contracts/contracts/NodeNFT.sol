//SPDX-License-Identifier: MIT
pragma solidity =0.8.18;

import '@openzeppelin/contracts/access/AccessControlEnumerable.sol';
import '@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol';

contract NodeNFT is ERC721URIStorage, AccessControlEnumerable {
    bytes32 public constant METADATA_SETTER_ROLE = keccak256('METADATA_SETTER_ROLE');
    bytes32 public constant MINTER_ROLE = keccak256('MINTER_ROLE');
    bytes32 public constant BURNER_ROLE = keccak256('BURNER_ROLE');
    uint256 public constant MAX_MINT_AMOUNT = 100; // mint max amount at once

    string public baseURI;
    uint256 public topId;

    event SetBaseURI(string indexed uri);
    event Minted(address indexed to, uint256 indexed id);
    event MintedBatch(
        address indexed to,
        uint256 indexed startId,
        uint256 indexed amount
    );
    event Burned(uint256 indexed id);

    constructor(string memory uri) ERC721('Node Sales NFT', 'NDS') {
        require(bytes(uri).length != 0, 'Empty base URI');
        baseURI = uri;
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
        emit SetBaseURI(uri);
    }

    function mint(address to) public onlyRole(MINTER_ROLE) {
        require(to != address(0), 'Zero recepient');

        uint256 id = ++topId;
        _mint(to, id);

        emit Minted(to, id);
    }

    function mintBatch(address to, uint256 amount) external onlyRole(MINTER_ROLE) {
        require(to != address(0), 'Zero recepient');
        require(amount != 0, 'Zero amount');
        require(amount <= MAX_MINT_AMOUNT, 'Max amount exceeded');
        uint256 startTopId = topId;

        for (uint256 i; i < amount; ) {
            mint(to);
            unchecked {
                ++i;
            }
        }

        emit MintedBatch(to, startTopId, amount);
    }

    function burn(uint256 id) public onlyRole(BURNER_ROLE) {
        _burn(id);

        emit Burned(id);
    }

    function supportsInterface(
        bytes4 interfaceId
    )
        public
        view
        virtual
        override(AccessControlEnumerable, ERC721URIStorage)
        returns (bool)
    {
        return super.supportsInterface(interfaceId);
    }

    function setTokenURI(
        uint256 tokenId,
        string memory _tokenURI
    ) external onlyRole(METADATA_SETTER_ROLE) {
        _setTokenURI(tokenId, _tokenURI);
    }

    function setBaseURI(string calldata uri) external onlyRole(METADATA_SETTER_ROLE) {
        require(bytes(uri).length != 0, 'Empty base URI');
        baseURI = uri;
        emit SetBaseURI(uri);
    }

    function _baseURI() internal view virtual override returns (string memory) {
        return baseURI;
    }

    function _transfer(address, address, uint256) internal virtual override {
        // NON TRANSFERABLE
        revert();
    }
}
