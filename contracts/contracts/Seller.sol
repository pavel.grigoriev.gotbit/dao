//SPDX-License-Identifier: MIT
pragma solidity =0.8.18;

import '@openzeppelin/contracts-upgradeable/token/ERC20/utils/SafeERC20Upgradeable.sol';
import '@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol';

import './SellerUtils.sol';
import './NodeNFT.sol';

contract Seller is OwnableUpgradeable {
    using SafeERC20Upgradeable for IERC20Upgradeable;

    // STATE VARIABLES

    NodeNFT public nft;
    IERC20Upgradeable public token;
    SellerUtils.NodeTier[] public tiers;
    bytes32 public merkleRoot;
    string public merkleLeavesLink;
    uint256 public start;
    uint256 public end;
    uint256 public privateSellDuration;
    address public collector;
    uint256 public nodesSoftCap;

    bool public collected;
    uint256 public totalSupply;
    uint256 public nodesSupplyTotal;
    uint256 public boughtNodesTotal;
    mapping(uint256 => uint256) public boughtSupplyAtTier;
    mapping(address => mapping(uint256 => uint256)) public boughtCapByUserAtTier;

    mapping(address => uint256) public boughtNodesByUser;
    mapping(address => uint256) public investmentByUser;
    address[] public users;

    //  MODIFIERS

    modifier beforeStart() {
        require(block.timestamp < start, SellerUtils.STARTED);
        _;
    }

    //  FUNCTIONS

    constructor() {
        _disableInitializers();
    }

    // external

    receive() external payable {
        emit SellerUtils.Received(msg.sender, msg.value);
    }

    /// @notice Inits a new contract
    function init(
        address _nft,
        address _token,
        bytes32 _merkleRoot,
        string calldata _leavesLink,
        uint256 _startTimestamp,
        uint256 _endTimestamp,
        uint256 _privateSellDuration,
        SellerUtils.NodeTier[] calldata _tiers,
        address _admin,
        address _collector
    ) external initializer {
        {
            require(_nft != address(0), SellerUtils.ZERO_ADDRESS);
            require(_startTimestamp > block.timestamp, SellerUtils.START_TOO_LOW);
            require(_endTimestamp > _startTimestamp, SellerUtils.TIMESTAMPS_CROSS);
            uint256 duration = _endTimestamp - _startTimestamp;
            require(duration >= SellerUtils.MIN_DURATION, SellerUtils.DURATION_TOO_LOW);
            require(duration <= SellerUtils.MAX_DURATION, SellerUtils.DURATION_TOO_HIGH);
            require(_merkleRoot != bytes32(0), SellerUtils.ZERO_ROOT);
            require(bytes(_leavesLink).length != 0, SellerUtils.EMPTY_LINK);
            require(_admin != address(0), SellerUtils.ZERO_ADMIN);
            require(_collector != address(0), SellerUtils.ZERO_COLLECTOR);
            require(
                _privateSellDuration <= duration,
                SellerUtils.PRIVATE_DURATION_TOO_HIGH
            );

            SellerUtils.validateTiers(_tiers);
        }

        {
            nft = NodeNFT(_nft);
            start = _startTimestamp;
            end = _endTimestamp;
            privateSellDuration = _privateSellDuration;
            merkleLeavesLink = _leavesLink;
            merkleRoot = _merkleRoot;
            _transferOwnership(_admin);
            collector = _collector;
            token = IERC20Upgradeable(_token); // if 0x0 => using native coin

            _setTiers(_tiers);
        }
        {
            emit SellerUtils.Init(_nft, _token, _merkleRoot);
        }
        {
            emit SellerUtils.SetTiers(_tiers);
        }
    }

    function setSoftCap(uint256 _nodesSoftCap) external onlyOwner beforeStart {
        // additional setup method
        require(_nodesSoftCap != 0, SellerUtils.ZERO_AMOUNT);
        require(_nodesSoftCap != nodesSoftCap, SellerUtils.DUPLICATE);

        nodesSoftCap = _nodesSoftCap;

        // event
        emit SellerUtils.SetSoftCap(_nodesSoftCap);
    }

    function resetNFT(address _nft) external onlyOwner beforeStart {
        require(_nft != address(0), SellerUtils.ZERO_ADDRESS);
        require(_nft != address(nft), SellerUtils.DUPLICATE);

        nft = NodeNFT(_nft);

        // event
        emit SellerUtils.ResetNFT(_nft);
    }

    function resetTiers(
        SellerUtils.NodeTier[] calldata _tiers
    ) external onlyOwner beforeStart {
        SellerUtils.validateTiers(_tiers);
        delete tiers;
        _setTiers(_tiers);
        // event
        emit SellerUtils.SetTiers(_tiers);
    }

    function resetStart(uint256 _start) external onlyOwner beforeStart {
        require(_start > block.timestamp, SellerUtils.START_TOO_LOW);
        require(_start != start, SellerUtils.DUPLICATE);

        uint256 _end = end;
        require(_end > _start, SellerUtils.TIMESTAMPS_CROSS);
        uint256 duration = _end - _start;
        require(duration >= SellerUtils.MIN_DURATION, SellerUtils.DURATION_TOO_LOW);
        require(duration <= SellerUtils.MAX_DURATION, SellerUtils.DURATION_TOO_HIGH);

        start = _start;
        // event
        emit SellerUtils.ResetStart(_start);
    }

    function resetEnd(uint256 _end) external onlyOwner beforeStart {
        uint256 startTimestamp = start;
        require(_end > startTimestamp, SellerUtils.TIMESTAMPS_CROSS);
        uint256 duration = _end - startTimestamp;
        require(duration >= SellerUtils.MIN_DURATION, SellerUtils.DURATION_TOO_LOW);
        require(duration <= SellerUtils.MAX_DURATION, SellerUtils.DURATION_TOO_HIGH);
        require(_end != end, SellerUtils.DUPLICATE);

        end = _end;
        // event
        emit SellerUtils.ResetEnd(_end);
    }

    function resetPrivateSelLDuration(
        uint256 _privateSellDuration
    ) external onlyOwner beforeStart {
        uint256 duration = end - start;

        require(_privateSellDuration <= duration, SellerUtils.PRIVATE_DURATION_TOO_HIGH);
        require(_privateSellDuration != privateSellDuration, SellerUtils.DUPLICATE);

        privateSellDuration = _privateSellDuration;

        // event
        emit SellerUtils.ResetPrivateSellDuration(_privateSellDuration);
    }

    function resetRoot(bytes32 _root) external onlyOwner beforeStart {
        require(_root != bytes32(0), SellerUtils.ZERO_ROOT);
        require(_root != merkleRoot, SellerUtils.DUPLICATE);
        merkleRoot = _root;
        // event
        emit SellerUtils.ResetRoot(_root);
    }

    function resetLeavesLink(string calldata _leavesLink) external onlyOwner {
        // can be reset during sell period due to possible storage failure (leaves link is not used for verification)
        require(bytes(_leavesLink).length != 0, SellerUtils.EMPTY_LINK);
        require(
            !SellerUtils.strCmp(_leavesLink, merkleLeavesLink),
            SellerUtils.DUPLICATE
        );

        merkleLeavesLink = _leavesLink;
        // event
        emit SellerUtils.ResetLeaves(_leavesLink);
    }

    function resetCollector(address _collector) external onlyOwner {
        require(block.timestamp <= end, SellerUtils.OVER);
        require(_collector != address(0), SellerUtils.ZERO_ADDRESS);
        require(_collector != collector, SellerUtils.DUPLICATE);
        collector = _collector;
        // event
        emit SellerUtils.ResetCollector(_collector);
    }

    function resetToken(IERC20Upgradeable _token) external onlyOwner beforeStart {
        // address 0x0 == using ETH
        require(_token != token, SellerUtils.DUPLICATE);
        token = _token;
        // event
        emit SellerUtils.ResetToken(address(_token));
    }

    function userClaim(address recepient) external {
        require(recepient != address(0), SellerUtils.ZERO_ADDRESS);
        require(block.timestamp > end, SellerUtils.NOT_OVER);
        require(boughtNodesTotal < nodesSoftCap, SellerUtils.SOFT_CAP_FULFILLED);

        uint256 invest =investmentByUser[msg.sender];
        require(invest != 0, SellerUtils.NOTHING_TO_COLLECT);

        emit SellerUtils.UserClaimed(recepient, invest);

        IERC20Upgradeable tokenLocal = token;

        if (address(tokenLocal) != address(0))
            tokenLocal.safeTransfer(recepient, invest);
        else SellerUtils.sendETHViaCall(payable(recepient), invest);
    }

    function collectFunds(address recepient) external {
        require(block.timestamp > end, SellerUtils.NOT_OVER);
        require(msg.sender == collector, SellerUtils.NOT_COLLECTOR);
        require(!collected, SellerUtils.ALREADY_COLLECTED);
        require(recepient != address(0), SellerUtils.ZERO_ADDRESS);

        uint256 supplyLocal = totalSupply;
        require(supplyLocal != 0, SellerUtils.NOTHING_TO_COLLECT);
        require(boughtNodesTotal >= nodesSoftCap, SellerUtils.SOFT_CAP_NOT_FULFILLED);

        collected = true;

        // event
        emit SellerUtils.ClaimedFunds(recepient, supplyLocal);

        IERC20Upgradeable tokenLocal = token;

        if (address(tokenLocal) != address(0))
            tokenLocal.safeTransfer(recepient, supplyLocal);
        else SellerUtils.sendETHViaCall(payable(recepient), supplyLocal);
    }

    function collectExtraFunds(
        address tokenToCollect,
        address recepient
    ) external returns (uint256) {
        require(msg.sender == collector, SellerUtils.NOT_COLLECTOR);
        require(recepient != address(0), SellerUtils.ZERO_ADDRESS);
        // token == 0x0 => collecting ETH

        uint256 lowerBoundOnContract;

        if (tokenToCollect == address(token)) {
            // if funds not collected => supply stays on contract
            if (!collected) lowerBoundOnContract = totalSupply;
        } // else collect the whole balance

        uint256 balance;

        if (tokenToCollect == address(0)) {
            // ETH
            balance = address(this).balance;
        } else {
            // ERC20
            balance = IERC20Upgradeable(tokenToCollect).balanceOf(address(this));
        }

        uint256 transferAmount = balance - lowerBoundOnContract;
        require(transferAmount != 0, SellerUtils.NOTHING_TO_COLLECT);

        // event
        emit SellerUtils.ClaimedExtraFunds(recepient, tokenToCollect, transferAmount);

        if (tokenToCollect == address(0)) {
            // ETH
            SellerUtils.sendETHViaCall(payable(recepient), transferAmount);
        } else {
            // ERC20
            IERC20Upgradeable(tokenToCollect).safeTransfer(recepient, transferAmount);
        }

        return transferAmount;
    }

    function purchaseNodes(
        uint256 startTier,
        uint256 nodesTargetAmount,
        bool mustFulfill,
        uint256 maxPurchaseCost,
        bytes32[] calldata merkleProof
    ) external payable {
        uint256 startLocal = start;
        require(
            startLocal <= block.timestamp && end >= block.timestamp,
            SellerUtils.NOT_IN_PROGRESS
        );

        // check address in case of a private sell period
        if (block.timestamp - startLocal <= privateSellDuration)
            SellerUtils.hasAccess(msg.sender, merkleProof, merkleRoot);

        (
            bool fulfilled,
            uint256[] memory buyAmountAtTier,
            uint256 totalPurchaseCost,
            uint256 nodesBought
        ) = simulatePurchaseNodes(msg.sender, startTier, nodesTargetAmount);

        require(nodesBought != 0, SellerUtils.ZERO_NODES_BOUGHT);

        // revert if fulfilled required but not possible
        if (mustFulfill) require(fulfilled, SellerUtils.NOT_FULFILLED);

        if (maxPurchaseCost != 0) {
            // check totalPurchaseCost
            require(totalPurchaseCost <= maxPurchaseCost, SellerUtils.MAX_PRICE_EXCEEDED);
        }

        if (address(token) == address(0)) {
            // native coin
            require(msg.value == totalPurchaseCost, SellerUtils.WRONG_COST);
        } else {
            // ERC20 token
            token.safeTransferFrom(msg.sender, address(this), totalPurchaseCost);
        }

        // change the state variables

        totalSupply += totalPurchaseCost;
        boughtNodesTotal += nodesBought;
        investmentByUser[msg.sender] += totalPurchaseCost;

        if (boughtNodesByUser[msg.sender] == 0) {
            // first invest
            users.push(msg.sender);
        }

        uint256 len = tiers.length;
        for (uint256 tierId = startTier; tierId < len; ) {
            uint256 buyAmount = buyAmountAtTier[tierId];
            if (buyAmount != 0) {
                boughtCapByUserAtTier[msg.sender][tierId] += buyAmount;
                boughtSupplyAtTier[tierId] += buyAmount;
                boughtNodesByUser[msg.sender] += buyAmount;

                nft.mintBatch(msg.sender, buyAmount);
            }

            unchecked {
                ++tierId;
            }
        }

        emit SellerUtils.PurchasedNodes(msg.sender, nodesBought, totalPurchaseCost);
    }

    // external view

    function checkAvailableSupplyPerTier(
        uint256 tierIndex
    ) external view returns (uint256) {
        require(tierIndex < tiers.length, SellerUtils.WRONG_INDEX);
        SellerUtils.NodeTier memory tier = tiers[tierIndex];
        return tier.supply - boughtSupplyAtTier[tierIndex];
    }

    function checkAvailableCapPerAddressAtTier(
        address user,
        uint256 tierIndex
    ) external view returns (uint256) {
        require(user != address(0), SellerUtils.ZERO_ADDRESS);
        require(tierIndex < tiers.length, SellerUtils.WRONG_INDEX);
        SellerUtils.NodeTier memory tier = tiers[tierIndex];
        return tier.capacityPerAddress - boughtCapByUserAtTier[user][tierIndex];
    }

    function getUserInfoSlice(
        uint256 offset,
        uint256 limit
    ) external view returns (SellerUtils.UserInfo[] memory res) {
        uint256 len = users.length;
        res = new SellerUtils.UserInfo[](len);
        require(offset + limit <= len, SellerUtils.INVALID_SLICE);
        require(limit != 0, SellerUtils.ZERO_LENGTH);

        for (uint256 i; i < limit; ) {
            address user = users[i + offset];
            res[i] = SellerUtils.UserInfo({
                user: user,
                nodesAmount: boughtNodesByUser[user]
            });
            unchecked {
                ++i;
            }
        }
    }

    function getUsersLength() external view returns (uint256) {
        return users.length;
    }

    function getTiersLength() external view returns (uint256) {
        return tiers.length;
    }

    function getTiersSlice(
        uint256 offset,
        uint256 limit
    ) external view returns (SellerUtils.NodeTier[] memory res) {
        uint256 len = tiers.length;
        res = new SellerUtils.NodeTier[](len);
        require(offset + limit <= len, SellerUtils.INVALID_SLICE);
        require(limit != 0, SellerUtils.ZERO_LENGTH);

        for (uint256 i; i < limit; ) {
            res[i] = tiers[i + offset];
            unchecked {
                ++i;
            }
        }
    }

    // public

    function simulatePurchaseNodes(
        address user,
        uint256 startTier,
        uint256 nodesTargetAmount
    )
        public
        view
        returns (
            bool fulfilled,
            uint256[] memory buyAmountAtTier,
            uint256 totalPurchaseCost,
            uint256 totalBought
        )
    {
        require(
            checkAvailableSupplyTotal() >= nodesTargetAmount,
            SellerUtils.NODES_TOTAL_EXCEEDED
        );
        require(
            nodesTargetAmount <= SellerUtils.MAX_BUY_AMOUNT,
            SellerUtils.MAX_AMOUNT_EXCEEDED
        );
        uint256 len = tiers.length;
        require(startTier < len, SellerUtils.WRONG_INDEX);
        require(user != address(0), SellerUtils.ZERO_ADDRESS);
        require(nodesTargetAmount != 0, SellerUtils.ZERO_AMOUNT);

        buyAmountAtTier = new uint256[](len); // [0, 0, ..., 0] - tiers.length

        for (uint256 tierId = startTier; tierId < len; ) {
            SellerUtils.NodeTier memory tier = tiers[tierId];
            uint256 availableSupplyAtTier = tier.supply - boughtSupplyAtTier[tierId];
            uint256 availableCapForUserAtTier = tier.capacityPerAddress -
                boughtCapByUserAtTier[user][tierId];

            uint256 buyAtThisTier;

            if (availableCapForUserAtTier <= availableSupplyAtTier) {
                // can stop at the current tier
                buyAtThisTier = availableCapForUserAtTier;
            } else {
                // must buy the rest of the supply, reduce cap and move to the next tier
                buyAtThisTier = availableSupplyAtTier;
            }

            if (totalBought + buyAtThisTier > nodesTargetAmount) {
                // must reduce the buy amount and stop purchase (will work only once)
                buyAtThisTier = nodesTargetAmount - totalBought;
                totalPurchaseCost += tier.pricePerNode * buyAtThisTier;
                buyAmountAtTier[tierId] = buyAtThisTier;
                totalBought += buyAtThisTier;
                break;
            }

            // can buy the whole amount
            totalPurchaseCost += tier.pricePerNode * buyAtThisTier;
            buyAmountAtTier[tierId] = buyAtThisTier;
            totalBought += buyAtThisTier;

            unchecked {
                ++tierId;
            }
        }

        // check if bought the whole targer amount

        // totalBought <= nodesTargetAmount due to function logic
        fulfilled = (totalBought == nodesTargetAmount);
    }

    function checkAvailableSupplyTotal() public view returns (uint256) {
        return nodesSupplyTotal - boughtNodesTotal;
    }

    function _setTiers(SellerUtils.NodeTier[] memory _tiers) private {
        uint256 supplyAllNodes;
        for (uint256 i; i < _tiers.length; ) {
            tiers.push(_tiers[i]);
            supplyAllNodes += _tiers[i].supply;
            unchecked {
                ++i;
            }
        }
        nodesSupplyTotal = supplyAllNodes;
    }
}
