//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import './Merkle.sol';

library SellerUtils {
    using Merkle for bytes32[];
    // TYPES

    struct NodeTier {
        uint256 pricePerNode;
        uint256 supply;
        uint256 capacityPerAddress;
    }

    struct UserInfo {
        address user;
        uint256 nodesAmount;
    }

    // CONSTANTS

    uint256 public constant MIN_DURATION = 1 hours;
    uint256 public constant MAX_DURATION = 10 * 365 days; // 10 years
    uint256 public constant MAX_TIERS_COUNT = 100;
    uint256 public constant GAS_FOR_CALL = 5000;
    uint256 public constant MAX_BUY_AMOUNT = 100; // max 100 nodes per buy (avoid gas limit per block)

    // ERROR CONSTANTS

    string public constant START_TOO_LOW = 'Start too low';
    string public constant TIMESTAMPS_CROSS = 'Timestamps cross';
    string public constant DURATION_TOO_LOW = 'Duration too low';
    string public constant DURATION_TOO_HIGH = 'Duration too high';
    string public constant ZERO_ROOT = 'Zero root';
    string public constant EMPTY_LINK = 'Empty link';
    string public constant CAP_ZERO = 'Cap Zero';
    string public constant ALLOC_TOO_LOW = 'Alloc too low';
    string public constant ZERO_PRICE = 'Zero price';
    string public constant CAP_REACHED = 'Zero price';
    string public constant ZERO_LENGTH = 'Zero length';
    string public constant LENGTH_TOO_HIGH = 'Length too high';
    string public constant ZERO_ADMIN = 'Zero admin';
    string public constant ZERO_COLLECTOR = 'Zero collector';
    string public constant NOT_COLLECTOR = 'Not collector';
    string public constant STARTED = 'Started';
    string public constant DUPLICATE = 'Duplicate';
    string public constant NOT_OVER = 'Not Over';
    string public constant ALREADY_COLLECTED = 'Already collected';
    string public constant NOTHING_TO_COLLECT = 'Nothing to collect';
    string public constant NODES_TOTAL_EXCEEDED = 'Nodes total exceeded';
    string public constant WRONG_INDEX = 'Wrong index';
    string public constant NOT_FULFILLED = 'Not fulfilled';
    string public constant WRONG_COST = 'Wrong cost';
    string public constant SEND_FAILED = 'Send failed';
    string public constant NOT_ALLOWED = 'Not allowed';
    string public constant ZERO_ADDRESS = 'Zero address';
    string public constant INVALID_SLICE = 'Invalid slice';
    string public constant ZERO_AMOUNT = 'Zero amount';
    string public constant WRONG_PROOF = 'Wrong proof';
    string public constant ZERO_NODES_BOUGHT = 'Zero nodes bought';
    string public constant NOT_IN_PROGRESS = 'Not in progress';
    string public constant MAX_PRICE_EXCEEDED = 'Max price exceeded';
    string public constant OVER = 'Over';
    string public constant MAX_AMOUNT_EXCEEDED = 'Max amount exceeded';
    string public constant PRIVATE_DURATION_TOO_HIGH = 'Private duration too high';
    string public constant SOFT_CAP_NOT_FULFILLED = 'Soft cap not fulfilled';
    string public constant SOFT_CAP_FULFILLED = 'Soft cap fulfilled';

    // EVENTS

    event Init(address indexed nft, address indexed token, bytes32 indexed root);

    event ResetNFT(address indexed nft);

    event ResetStart(uint256 indexed start);

    event ResetEnd(uint256 indexed end);

    event ResetPrivateSellDuration(uint256 indexed duration);

    event SetTiers(NodeTier[] indexed tiers);

    event ResetRoot(bytes32 indexed root);

    event ResetLeaves(string indexed link);

    event ResetCollector(address indexed collector);

    event ResetToken(address indexed token);

    event Received(address indexed from, uint256 indexed amount);

    event SetSoftCap(uint256 indexed softCap);

    event UserClaimed(address indexed to, uint256 indexed amount);

    event PurchasedNodes(
        address indexed user,
        uint256 indexed nodesAmount,
        uint256 indexed totalCost
    );

    event ClaimedFunds(address indexed to, uint256 indexed amount);

    event ClaimedExtraFunds(
        address indexed to,
        address indexed token,
        uint256 indexed amount
    );

    // FUNCTIONS

    function validateTiers(NodeTier[] memory _tiers) external pure {
        require(_tiers.length != 0, ZERO_LENGTH);
        require(_tiers.length <= MAX_TIERS_COUNT, LENGTH_TOO_HIGH);

        _validateTier(_tiers[0]);
        for (uint256 i = 1; i < _tiers.length; ) {
            _validateTier(_tiers[i]);
            unchecked {
                ++i;
            }
        }
    }

    function strCmp(string memory a, string memory b) external pure returns (bool) {
        return keccak256(bytes(a)) == keccak256(bytes(b));
    }

    function sendETHViaCall(address payable to, uint256 value) external {
        (bool sent, bytes memory data) = to.call{value: value, gas: GAS_FOR_CALL}('');
        require(sent, SEND_FAILED);
    }

    function hasAccess(
        address user,
        bytes32[] calldata proof,
        bytes32 root
    ) external view {
        // verify user address
        bytes32 leaf = keccak256(abi.encode(user));
        require(proof.verify(root, leaf), WRONG_PROOF);
    }

    function _validateTier(NodeTier memory _tier) private pure {
        require(_tier.capacityPerAddress != 0, CAP_ZERO);
        require(_tier.supply >= _tier.capacityPerAddress, ALLOC_TOO_LOW);
        require(_tier.pricePerNode != 0, ZERO_PRICE);
    }
}
