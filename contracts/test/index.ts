import { deployments, ethers } from 'hardhat'
import { deployProxy } from './proxyHelper';
import { BaseContract } from 'ethers';
import { NodeNFT, Seller, SellerUtils, Token } from '@/typechain-types';
import { Tier, disableInitializer } from './helper';
import { User, toLeaves } from './parse';
import { calculateRoot } from './merkle';
import { bigint } from 'hardhat/internal/core/params/argumentTypes';

export const deploy = async () => {
  const [deployer, collector, user1, user2, burner, metadataSetter] = await ethers.getSigners();

  const token = await ethers.deployContract("Token", ["Test Token", "TST"], deployer) as BaseContract as Token
  await token.waitForDeployment()

  const nft = await ethers.deployContract("NodeNFT", ['base-uri/'], deployer) as BaseContract as NodeNFT
  await nft.waitForDeployment()

  const sellerUtils = await ethers.deployContract('SellerUtils', deployer) as BaseContract as SellerUtils
  await sellerUtils.waitForDeployment()

  const seller = await ethers.deployContract("Seller", [], {
    libraries: {
      'SellerUtils': sellerUtils.target
    } 
  }) as BaseContract as Seller
  await seller.waitForDeployment()
  await disableInitializer(seller.target)

  const MINTER_ROLE = await nft.MINTER_ROLE()
  await nft.grantRole(MINTER_ROLE, seller.target)

  await nft.revokeRole(await nft.DEFAULT_ADMIN_ROLE(), deployer.address)

  const users = [deployer.address, user1.address, user2.address];
  const leaves = toLeaves(users as User[])
  const root = calculateRoot(leaves)

  const block = await ethers.provider.getBlock('latest')
  const now = BigInt(Date.now()) / 1000n

  const start = (BigInt(block?.timestamp || now)) + 3600n
  const end = start + 86400n

  let tiers: Tier[] = []

  const ONE = ethers.WeiPerEther

  tiers.push({
    supply: 100n,
    capacityPerAddress: 5n,
    pricePerNode: ONE
  } as Tier)

  tiers.push({
    supply: 50n,
    capacityPerAddress: 10n,
    pricePerNode: ONE * 5n
  } as Tier)

  tiers.push({
    supply: 10n,
    capacityPerAddress: 10n,
    pricePerNode: ONE * 10n
  } as Tier)

  await seller.connect(deployer).init(nft.target, token.target, root, 'leaves/link', start, end, (end - start) / 2n, tiers, deployer.address, collector.address)

  // Fixtures can return anything you consider useful for your tests
  return { signers: [deployer, collector, user1, user2], token, seller, root, leaves, tiers, libraries: {sellerUtils}, nft };
}