import { ethers } from 'ethers'

export type User = string

const defaultAbiCoder = new ethers.AbiCoder()

export const toLeaves = (users: User[]) =>
  users.map((u) =>
    ethers.keccak256(defaultAbiCoder.encode(['address'], [u]))
  )

export const toLeaf = (user: User) =>
  ethers.keccak256(defaultAbiCoder.encode(['address'], [user]))

