import { token } from '@/typechain-types/@openzeppelin/contracts'
import { HardhatEthersSigner } from '@nomicfoundation/hardhat-ethers/signers'
import { abi as ProxyAdminABI, bytecode as ProxyAdminBytecode } from '@openzeppelin/contracts/build/contracts/ProxyAdmin.json'
import { abi as TransparentUpgradeableProxyABI, bytecode as TransparentUpgradeableProxyBytecode } from '@openzeppelin/contracts/build/contracts/TransparentUpgradeableProxy.json'
import { AddressLike } from 'ethers'
import { ethers } from 'hardhat'

export const deployProxy = async (name: string, proxyOwner: AddressLike, signer: HardhatEthersSigner, args: any[], libraries: any, methodName: string = 'init', proxyName: string = 'TransparentUpgradeableProxy') => {
    // deploy proxy implementation
    const contractInstance = await ethers.deployContract(name, {
        from: signer.address,
        libraries
    })
    await contractInstance.waitForDeployment()

    const encodedInitCall = contractInstance.interface.encodeFunctionData(methodName, args)

    // deploy proxy contract
    const transparentUpgradeableProxyFactory = new ethers.ContractFactory(TransparentUpgradeableProxyABI, TransparentUpgradeableProxyBytecode)
    const proxy = await transparentUpgradeableProxyFactory.connect(signer).deploy(await contractInstance.getAddress(), proxyOwner, encodedInitCall)
    await proxy.waitForDeployment()

    return { implementation: contractInstance, proxy }
}