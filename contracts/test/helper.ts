import { ethers, network } from 'hardhat';
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';
import { randomBytes } from 'crypto';
import { expect } from 'chai';
import { HardhatEthersSigner } from '@nomicfoundation/hardhat-ethers/signers';
import { AddressLike, BytesLike, hexlify, toQuantity, zeroPadBytes, zeroPadValue } from 'ethers';

export const randomAddress = () => {
    const id = randomBytes(32).toString("hex");
    const privateKey = "0x" + id;
    const wallet = new ethers.Wallet(privateKey);
    return wallet.address;
};

export const randomHash = () => {
    return '0x' + randomBytes(32).toString("hex")
};

export const addSigner = async (
    address: string
): Promise<HardhatEthersSigner> => {
    await network.provider.request({
        method: "hardhat_impersonateAccount",
        params: [address],
    });
    await network.provider.send("hardhat_setBalance", [
        address,
        "0x1000000000000000000",
    ]);
    return await ethers.getSigner(address);
};

export const removeSigner = async (address: string) => {
    await network.provider.request({
        method: "hardhat_stopImpersonatingAccount",
        params: [address],
    });
};

export const useSigner = async (
    address: string,
    f: (signer: HardhatEthersSigner) => Promise<void>
) => {
    const signer = await addSigner(address);
    await f(signer);
    await removeSigner(address);
};

export const sleepTo = async (timestamp: bigint) => {
    await network.provider.send("evm_setNextBlockTimestamp", [
        Number(timestamp),
    ]);
    await network.provider.send("evm_mine");
};

export const sleep = async (seconds: bigint) => {
    await network.provider.send("evm_increaseTime", [
        Number(seconds),
    ]);
    await network.provider.send("evm_mine");
};

export const abs = (n: bigint) => (n < 0n) ? -n : n;

export const epsEqual = (
    a: bigint,
    b: bigint,
    eps: bigint = 1n,
    decimals: bigint = 10_000n
) => {
    if (a == b) return true;
    if (a == 0n) return b <= eps
    // |a - b| / a < eps <==> a ~ b
    const res = (abs(a - b) * decimals / a) < eps
    if (!res) console.log(`A = ${a}, B = ${b}`)
    return res
}

export async function disableInitializer(contract: AddressLike) {
    const INITIALIZERS_SLOT = 0
    const value = ethers.ZeroHash // 32 zeros like 0x00....00

    await ethers.provider.send('hardhat_setStorageAt', [
        contract,
        toQuantity(INITIALIZERS_SLOT),
        value,
    ])
}

/*
struct NodeTier {
    uint256 pricePerNode;
    uint256 supply;
    uint256 capacityPerAddress;
}
*/
export type Tier = {
    pricePerNode: bigint,
    supply: bigint,
    capacityPerAddress: bigint
}
