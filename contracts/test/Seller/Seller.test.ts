import { ethers } from 'hardhat'
import { expect, use } from 'chai'

import { deploy } from '@/test'
import { NodeNFT, Seller, SellerUtils, Seller__factory, Token } from '@/typechain-types'
import { ERRORS } from './Errors'
import { Tier, disableInitializer, randomAddress, randomHash, sleep, sleepTo, useSigner } from '../helper'
import { epsEqual } from '../helper'
import { HardhatEthersSigner } from '@nomicfoundation/hardhat-ethers/signers'
import { loadFixture } from "@nomicfoundation/hardhat-toolbox/network-helpers";
import { Addressable, ZeroAddress, ZeroHash } from 'ethers'
import { Address } from 'hardhat-deploy/types'
import { calculateRoot, generateProof } from '../merkle'
import { toLeaf, toLeaves } from '../parse'
import { boolean } from 'hardhat/internal/core/params/argumentTypes'
import { randomBytes } from 'crypto'

describe('Seller contract', () => {
  let deployer: HardhatEthersSigner
  let collector: HardhatEthersSigner
  let user1: HardhatEthersSigner
  let user2: HardhatEthersSigner
  let seller: Seller
  let token: Token
  let root: string
  let leaves: string[]
  let tiers: Tier[]
  let leavesLink = 'leaves/link'
  let start: bigint
  let end: bigint
  let sellerFactory: Seller__factory
  let sellerUtils: SellerUtils
  const ONE = ethers.WeiPerEther
  let nft: NodeNFT

  beforeEach(async () => {
    const deployState = await loadFixture(deploy)

      ;[deployer, collector, user1, user2] = deployState.signers
    token = deployState.token
    seller = deployState.seller
    root = deployState.root
    leaves = deployState.leaves
    tiers = deployState.tiers
    nft = deployState.nft

    sellerUtils = deployState.libraries.sellerUtils

    sellerFactory = await ethers.getContractFactory('Seller', {
      libraries: {
        'SellerUtils': sellerUtils.target as Address
      }
    }) as Seller__factory

    const block = await ethers.provider.getBlock('latest')
    const now = BigInt(Date.now()) / 1000n

    //start = (BigInt(block?.timestamp || now)) + 3600n
    start = await seller.start()
    end = start + 86400n

    await token.mint(deployer.address, ONE * 1_000_000n)
    await token.mint(user1.address, ONE * 1_000_000n)
    await token.mint(user2.address, ONE * 1_000_000n)

    await token.connect(deployer).approve(seller.target, ONE * 1_000_000n)
    await token.connect(user1).approve(seller.target, ONE * 1_000_000n)
    await token.connect(user2).approve(seller.target, ONE * 1_000_000n)
  })

  const compareTiers = (a: Tier[], b: Tier[]) => {
    expect(a.length).to.eq(b.length)

    for (let i = 0; i < a.length; ++i) {
      expect(a[i].supply).to.eq(b[i].supply)
      expect(a[i].capacityPerAddress).to.eq(b[i].capacityPerAddress)
      expect(a[i].pricePerNode).to.eq(b[i].pricePerNode)
    }
  }

  const findLeaf = (account: string, leaves: string[]) => {
    for (let i = 0; i < leaves.length; ++i) {
      const curLeaf = toLeaf(account)
      if (leaves[i] === curLeaf) return { found: true, index: i, curLeaf }
    }
    return { found: false, index: -1, undefined }
  }

  const balance = async (addr: string | Addressable, tokenContract: Token = token) => await tokenContract.balanceOf(addr)

  const ethBalance = async (addr: string | Addressable) => {
    return await ethers.provider.getBalance(addr)
  }

  it('deploys', async () => {
    const leaf = findLeaf(user1.address, leaves).curLeaf || ''
    await sleepTo(start)
    await seller.connect(user1).purchaseNodes(0n, 5n, true, 0n, generateProof(leaves, leaf))

    expect(await seller.boughtNodesByUser(user1.address)).to.eq(5)
    expect(await balance(seller.target)).to.eq(tiers[0].pricePerNode * 5n)
    expect(await seller.boughtNodesTotal()).to.eq(5n)
    expect(await seller.boughtCapByUserAtTier(user1.address, 0)).to.eq(5n)

    expect(await nft.balanceOf(user1.address)).to.eq(5)

    for (let i = 1; i <= 5; ++i) {
      expect(await nft.ownerOf(i)).to.eq(user1.address)
    }

    await sleepTo(end + 10n)

    const wallet = randomAddress()
    await seller.connect(collector).collectFunds(wallet)

    expect(await token.balanceOf(wallet)).to.eq(5n * tiers[0].pricePerNode)
  })
})
